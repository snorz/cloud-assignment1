const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
const bodyParser= require('body-parser')
const app = express()


const ObjectId = require("mongodb").ObjectId;

// Make sure you place body-parser before your CRUD handlers!
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.json());
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://simadm:Nj9ClyLOppwmjC6N@cluster0.cjrcd.mongodb.net/testDatabase?retryWrites=true&w=majority";
//const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

const assert = require('assert');


const DATABASE_NAME = "testDatabase";
const COLLECTION_NAME = "customers";




MongoClient.connect(uri, {useUnifiedTopology: true})
  .then(client => {
    console.log('connected to database')
    const db = client.db(DATABASE_NAME)
    const userCollection = db.collection(COLLECTION_NAME)

  //})


// this function lists checks if a collection exist in the database
db.listCollections().toArray(function(err, names) { // find all collections
    // if collection does not exist, create collection with jsonScheme restrictions
  if (!names.some(item => item.name === COLLECTION_NAME)){

      // create collection
    db.createCollection(COLLECTION_NAME, {
    validator: {
      $jsonSchema: {
        required: [
        "personal_number",
        "account_number",
        "first_name",
        "last_name",
        "date_of_birth",
        "city",
        "created_date",
        ],
        bsonType: "object",
        properties: {

          personal_number: { //Only accepts valid personal numbers
           bsonType: "string",
           pattern: "^(0[1-9]|[1-2][0-9]|31(?!(?:0[2469]|11))|30(?!02))(0[1-9]|1[0-2])\\d{7}$",
           description: "personal number",
          },
          account_number: {
            bsonType: "string",
            description: "auto generated account number",
          },
          first_name: {
            bsonType: "string",
            description: "First name of customer",
          },
          last_name: {
            bsonType: "string",
            description: "Last name of customer",
          },
          date_of_birth: {
            bsonType: "string",
            description: "user date of birth",
          },
          city: {
            bsonType: "string",
            description: "City the customer lives"
          },
          created_date: {
            bsonType: "double", //Because it is non negative and a large number
            description: "date of creation",
          },
        }
      }
    }
    });
  };

});

    app
      .use(express.static(path.join(__dirname, 'public')))
      .set('views', path.join(__dirname, 'views'))
      .set('view engine', 'ejs')
      .get('/', (req, res) => res.render('pages/index'))
      .get('/create', (req, res) => res.render('pages/create'))
      .get('/measurelatency/', (req, res) => {
        var d = new Date();
        var t2 = d.getTime();
          userCollection.findOne({"personal_number": "1" }) //We use the same query every time so that we can measure latency with the same payload each time
              .then(result => {
                var e = new Date();
                var t4 = e.getTime();
                var obj = { 't2': t2, 't4': t4 }
                  res.json(obj)
              })
          })

      .get('/users/:personal_number', function(req, res) {
        var result = "";
        userCollection.findOne({"personal_number": req.params.personal_number }, (error, result) => {
            if(!result) {
                return res.status(500).json("User not found");
            }
            else {
            //console.log(result.first_name)
            var createdDate = new Date(result.created_date) //.toLocaleTimeString("no-NO") //Convert back to date
            res.render('pages/users', {
              pnum: result.personal_number,
              fname: result.first_name,
              lname: result.last_name,
              dob: result.date_of_birth,
              ci: result.city,
              an: result.account_number,
              cr: createdDate
            });
          }})
        })

      .get('/api/check_customer_exists/:personal_number', (req, res) => {
          userCollection.findOne({"personal_number": req.params.personal_number })
              .then(result => {
                if (result) {
                  res.json('User exits')

                }else{
                  res.json("User does not exist")
                }
              })
          })

      .get('/api/get_customer_info/:personal_number', (req, res) => {
          userCollection.findOne({"personal_number": req.params.personal_number }, (error, result) => {
              if(error) {
                  return res.status(500).json(error);
              }
              res.json(result);
          })
      })

    // Insert customer details to customer table, return the customer account number
      .post('/api/insert_customer_details', (req, res) => {
        //req.body.account_number =
        userCollection.findOne({"personal_number": req.body.personal_number })
            .then(result => {
              if (result) {
                res.json('A user with this personal number already exits')
              }else{
                if (!req.body.personal_number || !req.body.first_name || !req.body.last_name || !req.body.date_of_birth || !req.body.city) {
                          res.status(400).send({ message: "Please fill inn all forms, can not be empty!" }); }
                var d = new Date();
                var cdate = d.getTime(); //Getting time
                var f = (cdate % 10000)  //For use in account_number
                req.body.created_date= cdate;
                req.body.account_number= (req.body.first_name + f); //Account number generation

                userCollection.insertOne(req.body)
                  .then(result => {
                    if(result) {
                      res.json("New user account created with account number: " + req.body.account_number)
                    } else {
                      res.json("Error, could not insert new user")
                    }
                  })
                  .catch(error => console.log(error))
                  console.log(req.body)
              }
            })
        })

      // update customer details to customer table
      .put('/api/update_customer_details', (req, res) => {
        userCollection.findOne({"personal_number": req.body.personal_number })
            .then(result => {
              if (result) {
                var fname = (req.body.first_name) ? req.body.first_name : result.first_name;
                var lname = (req.body.last_name) ? req.body.last_name : result.last_name;
                var dob = (req.body.date_of_birth) ? req.body.date_of_birth : result.date_of_birth;
                var city = (req.body.city) ? req.body.city : result.city;
                userCollection.findOneAndUpdate({
                  "personal_number": (req.body.personal_number) },
                  {
                    $set: {
                      first_name: fname,
                      last_name: lname,
                      date_of_birth: dob,
                      city: city
                    }
                  },
                  {
                    upsert:true
                  }
                  )
                  .then(result => res.json('Success!'))
                  .catch(error => console.error(error))
              }else{
                res.json("User does not exist")
              }
            })
          })

      // delete customer details to customer table
      .delete('/api/delete_customer_details/:personal_number', (req, res) => {
        console.log(req.body)
         userCollection.deleteOne({"personal_number": req.params.personal_number })
          .then(result => {
          if (result.deletedCount === 0){
            res.json("User not found, none deleted")
          }else{
          res.json('Successfully deleted user with personal number: ' + req.params.personal_number)
          }
        })
      })


      .listen(PORT, () => {
        console.log(`Listening on ${ PORT }`)})
})
