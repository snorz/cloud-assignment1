
# Design

The site layout consist of the frontpage (index) where the user can either input a personal number or check the latency breakdown of the system.
When you input your personal number it checks if it is a valid number according to how a personal number should be structured (see https://no.wikipedia.org/wiki/F%C3%B8dselsnummer for more information)

If the user already has a profile, they will be directed to their site /users/"Personal number" where they can edit their profile. If they do not have a profile, the user is redirected to a page where the user can create an account.

All the accounts can be access with this URI: /users/"Insert personal number"




### URL to heroku deployment: https://fierce-depths-54251.herokuapp.com/




## API
The application also has a REST API with the following operations:

### GET
/api/check_customer_exists/"Insert personal number"

/api/get_customer_info/"Insert personal number"

### POST
/api/insert_customer_details

### DELETE
/api/delete_customer_details/"Insert personal number"

### PUT
/api/update_customer_details




			
# Examples of some of the functionality
**curl -X GET https://fierce-depths-54251.herokuapp.com/api/check_customer_exists/1234**
Should return "User does not exist"
**curl -X GET https://fierce-depths-54251.herokuapp.com/api/check_customer_exists/13049437621**
Should return "User exists"

**curl -X POST -H 'content-type:application/json' -d '{"personal_number":"13049437621","first_name":"John","last_name":"Doe","date_of_birth":"2016-05-05","city":"Bergen"}' https://fierce-depths-54251.herokuapp.com/api/insert_customer_details**
should return "A user with this personal number already exits" This is because the user is already in the database

**curl -X DELETE https://fierce-depths-54251.herokuapp.com/api/delete_customer_details/13049437621**
should return "Successfully deleted user with personal number: 13049437621". If done again it should return "User not found, none deleted"

To insert John Doe back to the database, simply rerun the post command previously entered and you will get an response like: "New user account created with account number: JohnXXXX" where XXXX is numbers.

To update John's laste name from Doe to Hoe
**curl -X PUT -H 'content-type:application/json' -d '{"personal_number":"13049437621","last_name":"Hoe"}' https://fierce-depths-54251.herokuapp.com/api/update_customer_details**

If by mistake you enter an personal number not found in the database, it should return "User does not exist"



